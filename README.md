# Boilerplate MEAN Stack

A first step to MongoDB Express Angular(2) NodeJS applications. Used primarily as an API backend with admim panels, but could be extended to anything.

# Getting Started

**_Dependencies:_**
*MongoDB
*NodeJS

## Server Setup
Run:
`cd server`
`npm install`
_Copy '.env.example' into a new file called '.env'_
`npm start`

the server should be running at `localhost:3000`


## Client Setup
Run:
`npm install`
`npm start`
to get started

# To Add
* **Core:**
  * Cross platform build scripts
  * Tests
  * Comments (Documentation)
  * README (Documentation)
  * Add client (Angular)
  * Email reset password auth branch
  * Favicon
* **Branches:**
  * Localization
  * Role Authentication
  * Stripe/PayPal payment
  * Realtime (socket io?)
  * Social Login
* **Research:**
  * Does JWT need to be encrypted when stored clientside?

# Creator / Maintainer

Repository was created and is being maintained by Brayden Stewart.

# License

This project under [Unlicense](http://unlicense.org/).