import mongoose from 'mongoose'
import chai from 'chai'
import chaiHttp from 'chai-http'

import server from '../src/app'
import User from '../src/users/user-model'
import { Config } from '../src/helpers'

let should = chai.should();

chai.use(chaiHttp);

let validUser = {
  email: "andy@example.com",
  password: "testtest",
  confirmPassword: "testtest"
}

describe('Users', () => {
  console.log("\n", process.env.NODE_ENV, "\n", Config.MONGO_URL, "\n");
  beforeEach((done) => {
    User.remove({}, (err) => {
      done();
    });
  });
  describe('/POST user/register', () => {
    it('it should create a user.', (done) => {
      chai.request(server)
        .post('/api/user/register')
        .send(validUser)
        .end((err, res) => {
          should.not.exist(err);
          res.should.have.status(200);
          res.should.be.a('object');
          res.body.should.have.property('success').eql(true);
          res.body.should.have.deep.property('data.email').and.is.not.empty;
          res.body.should.have.deep.property('data._id').and.is.not.empty;
          res.body.should.not.have.deep.property('data.password');
          done();
        });
    });
  });
  describe('/POST user/register', () => {
    it('it should not allow a duplicate user.', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/register')
          .send(validUser)
          .end((err, res) => {
            should.exist(err);
            res.should.have.status(400);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(false);
            res.body.should.have.property('error');
            done();
          });
      })
    });
  });
  describe('/POST user/register', () => {
    it('it should not allow an invalid email.', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/register')
          .send({
            email: "andyexample.com",
            password: "testtest",
            confirmPassword: "testtest"
          })
          .end((err, res) => {
            should.exist(err);
            res.should.have.status(400);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(false);
            res.body.should.have.property('error');
            done();
          });
      })
    });
  });
  describe('/POST user/register', () => {
    it('it should not allow an invalid password.', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/register')
          .send({
            email: "andy@example.com",
            password: "tesest",
            confirmPassword: "tesest"
          })
          .end((err, res) => {
            should.exist(err);
            res.should.have.status(400);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(false);
            res.body.should.have.property('error');
            done();
          });
      })
    });
  });
  describe('/POST user/register', () => {
    it('it should not allow a non-matching password and confirmation.', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/register')
          .send({
            email: "andy@example.com",
            password: "testtest",
            confirmPassword: "testttest"
          })
          .end((err, res) => {
            should.exist(err);
            res.should.have.status(400);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(false);
            res.body.should.have.property('error');
            done();
          });
      })
    });
  });

  describe('/POST user/authenticate', () => {
    it('it should authenticate the user and give JWT', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/authenticate')
          .send({
            email: "andy@example.com",
            password: "testtest",
          })
          .end((err, res) => {
            should.not.exist(err);
            res.should.have.status(200);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.deep.property('data.token');
            done();
          });
      })
    });
  });

  describe('/GET user/logout', () => {
    it('it should logout the user', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/authenticate')
          .send({
            email: "andy@example.com",
            password: "testtest",
          })
          .end((err2, res) => {
            chai.request(server)
              .get('/api/user/logout')
              .end((err2, res2) => {
                should.not.exist(err2);
                res2.should.have.status(200);
                res2.should.be.a('object');
                res2.body.should.have.property('success').eql(true);
                res2.body.should.have.property('data');
                done();
              })
          });
      })
    });
  });

  describe('/GET user', () => {
    it('it should show data of an authenticated user', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .post('/api/user/authenticate')
          .send({
            email: "andy@example.com",
            password: "testtest",
          })
          .end((err, res) => {
            chai.request(server)
              .get('/api/user')
              .set('Authorization', res.body.data.token)
              .end((err2, res2) => {
                should.not.exist(err2);
                res2.should.have.status(200);
                res2.should.be.a('object');
                res2.body.should.have.property('success').eql(true);
                res2.body.should.have.deep.property('data.email').and.is.not.empty;
                res2.body.should.have.deep.property('data._id').and.is.not.empty;
                res2.body.should.not.have.deep.property('data.password');
                done();
              })
          });
      })
    });
  });
  describe('/GET user', () => {
    it('it should not allow unauthenticated person to view the user object.', (done) => {
      new User(validUser).save().then(user => {
        chai.request(server)
          .get('/api/user')
          .end((err, res) => {
            should.exist(err);
            res.should.have.status(401);
            res.should.be.a('object');
            res.body.should.have.property('success').eql(false);
            res.body.should.have.property('error');
            done();
          });
      })
    });
  });

});