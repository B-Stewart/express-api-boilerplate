import City from './city-model'
import { respondHelper } from '../helpers'

const controller = {}

controller.get = (req, res) => {
  City.findOne({_id: req.params.id}).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

controller.getAll = (req, res) => {
  City.find({}).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

controller.create = (req, res) => {
  //Validations
  req.checkBody('name', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)

    const { name } = req.body

    const city = new City({
      name
    })

    city.save().then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
  })
}

controller.update = (req, res) => {
  //Validations
  const allowedFields = ['name']
  const updatedFields = respondHelper.cleanParams(req.body, allowedFields)

  City.findByIdAndUpdate(req.params.id, updatedFields).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

controller.delete = (req, res) => {
  City.findByIdAndRemove(req.params.id).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

export default controller