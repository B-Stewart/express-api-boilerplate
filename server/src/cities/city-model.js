import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const { Schema } = mongoose

const schema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: [5, 'Name must be 5 characters or more']
  }
})

schema.plugin(uniqueValidator)

const City = mongoose.model('City', schema)

export default City