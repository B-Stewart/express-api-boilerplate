import express from 'express'
import passport from 'passport'

import cityController from './city-controller'

const routes = express.Router()

routes.get('/cities/:id', cityController.get)

routes.get('/cities', cityController.getAll)

routes.post('/cities', passport.authenticate('jwt', { session: false }), cityController.create)

routes.patch('/cities/:id', passport.authenticate('jwt', { session: false }), cityController.update)

routes.delete('/cities/:id', passport.authenticate('jwt', { session: false }), cityController.delete)

export default routes