import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'
import { isEmail } from 'validator'
import uniqueValidator from 'mongoose-unique-validator'

import { respondHelper } from '../helpers'

const { Schema } = mongoose

const SALT_FACTOR = 10

const schema = new Schema({
  email: {
    type: String,
    required: true,
    lowercase: true,
    unique: true,
    validate: [{ validator: value => isEmail(value), msg: 'Email is not a valid email address' }]
  },
  password: {
    type: String,
    required: true,
    minlength: [8, "Password must be at least 8 characters"]
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date
})

schema.options.toJSON = {
  transform: function(doc, obj, options) {
    delete obj.password
    delete obj.__v
    return obj
  }
}

schema.pre('save', function(next) {
  let user = this

  if(user.isModified('password') || user.isNew) {
    
    bcrypt.genSalt(SALT_FACTOR, (err, salt) => {
      if(err)
        return next(err)

      bcrypt.hash(user.password, salt, (err, hash) => {
        if(err)
          return next(err)
        user.password = hash
        next()
      })
    })
  }
  else
    next()
})

schema.methods.comparePassword = function(pw, cb) {
  bcrypt.compare(pw, this.password, (err, isMatch) => {
    if(err)
      return cb(err)
    cb(null, isMatch)
  })
}

schema.plugin(uniqueValidator)

const User = mongoose.model('User', schema)

export default User