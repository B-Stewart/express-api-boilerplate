import jwt from 'jsonwebtoken'
import crypto from 'crypto'

import smtpTransport from '../middleware/nodemailer-smtp'
import User from './user-model'
import { respondHelper } from '../helpers'

const controller = {}

controller.create = (req, res) => {
  //Validations
  req.checkBody('email', 'required').notEmpty()
  req.checkBody('password', 'required').notEmpty()
  req.checkBody('confirmPassword', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)

    const { email, password, confirmPassword } = req.body

    if(password !== confirmPassword)
      return respondHelper.error("Password and confirmation password do not match.", res)

    const user = new User({
      email,
      password 
    })

    user.save().then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
  })
}

controller.authenticate = (req, res) => {
  //Validations
  req.checkBody('email', 'required').notEmpty()
  req.checkBody('password', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)

      let { email, password } = req.body

      email = email.toLowerCase()

      User.findOne({ email }).then(user => {
        if(!user)
          return respondHelper.error("Authentication failed. User not found.", res)
        else
          user.comparePassword(password, (err, isMatch) => {
            if(isMatch && !err) {
              let token = jwt.sign(user, process.env.JWT_SECRET, {
                expiresIn: 10080 //in seconds, lasts for a week.
              })
              return respondHelper.success({token: `JWT ${token}`}, res)
            }
            else
              return respondHelper.error("Authentication failed. Passwords did not match", res)
          })
      })
  })
}

controller.logout = (req, res) => {
  req.logout()
  return respondHelper.success("Logged Out", res)
}

controller.getCurrent = (req, res) => {
  return respondHelper.success(req.user, res)
}

controller.forgotPassword = (req, res) => {
  //Validations
  req.checkBody('email', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)

    let { email } = req.body

    email = email.toLowerCase()

    User.findOne({ email }).then(user => {
      
      if(!user)
        return respondHelper.error("No account with that email address exists.", res)

      crypto.randomBytes(20, (err, buf) => {
        if(err)
          return respondHelper.error(err, res)

        let token = buf.toString('hex')

        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000 // 1 hour

        user.save().then(result => {

          let mailOptions = {
            to: user.email,
            from: process.env.ZOHO_USER,
            subject: 'Node.JS Password Reset',
            text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
            'http://' + req.headers.host + 'api/user/reset-password/' + token + '\n\n' +
            'If you did not request this, please ignore this email and your password will remain unchanged.\n'
          }

          smtpTransport.sendMail(mailOptions, err => {
            if(err)
              return respondHelper.error(err, res)
            else
              return respondHelper.success("Email Sent.", res)
          })

        }).catch(err => respondHelper.error(err, res))
      })
    })
  })
}

controller.resetPassword = (req, res) => {
  //Validations
  req.checkBody('password', 'required').notEmpty()
  req.checkBody('confirmPassword', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)
    
    const { token } = req.params
    const { password, confirmPassword } = req.body

    User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: Date.now() } }).then(user => {
      if(!user)
        return respondHelper.error("Password reset token is invalid or has expired.", res)

      //Set the new password and reset the reset fields
      user.password = password
      user.resetPasswordToken = undefined
      user.resetPasswordExpires = undefined

      user.save().then(result => {
        let mailOptions = {
          to: user.email,
          from: process.env.ZOHO_USER,
          subject: 'Your password has been changed',
          text: 'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
        }

        smtpTransport.sendMail(mailOptions, err => {
          if(err)
            return respondHelper.error(err, res)
          else
            return respondHelper.success("Email Sent.", res)
        })
        
        return respondHelper.success(result, res)
      }).catch(err => respondHelper.error(err, res))
    })
  })  
}

export default controller