import express from 'express'
import passport from 'passport'

import userController from './user-controller'
import respondHelper from '../helpers/respond-helper'
import bruteforce from '../middleware/bruteforce'

const routes = express.Router()

routes.post('/user/register', userController.create)

routes.post('/user/authenticate', bruteforce.prevent, userController.authenticate)

routes.get('/user/logout', userController.logout)

routes.get('/user', passport.authenticate('jwt', { session: false, failWithError: true } ), userController.getCurrent, respondHelper.routeError)

routes.post('/user/forgot-password', userController.forgotPassword)

routes.post('/user/reset-password/:token', userController.resetPassword)

export default routes