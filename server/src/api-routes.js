import express from 'express'

import cityRoutes from './cities/city-routes'
import heroRoutes from './heros/hero-routes'
import userRoutes from './users/user-routes'

const routes = express.Router()

routes.get('/', (req, res) => {
  return res.json({
    message: 'Welcome to our API!'
  })
})

export default [
  cityRoutes, 
  heroRoutes,
  userRoutes,
  routes
]