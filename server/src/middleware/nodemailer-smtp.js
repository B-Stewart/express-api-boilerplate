import nodemailer from 'nodemailer'

const smtpTransport = nodemailer.createTransport({
  service: 'Zoho',
  auth: {
    user: process.env.ZOHO_USER,
    pass: process.env.ZOHO_PASS
  }
})

export default smtpTransport