import ExpressBrute from 'express-brute'
import ExpressBruteMongo from 'express-brute-mongo'
import { MongoClient } from 'mongodb'

import { Config } from '../helpers'

//Set up mongo store for express-brute
let mongoBruteStore = new ExpressBruteMongo(ready => {
  MongoClient.connect(Config.MONGO_URL, (err, db) => {
    if (err) throw err
    ready(db.collection('bruteforce-store'))
  })
})

let options = process.env.NODE_ENV == 'test' ? {
	freeRetries: 10000,
	minWait: 0, // 5 minutes
	maxWait: 1, // 1 hour,
} : {}

let bruteforce = new ExpressBrute(mongoBruteStore, options)

export default bruteforce