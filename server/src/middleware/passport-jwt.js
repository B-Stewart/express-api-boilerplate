import passport from 'passport'
import { Strategy, ExtractJwt } from 'passport-jwt'

import User from '../users/user-model'

let passportJwt =  new Strategy({
  jwtFromRequest:ExtractJwt.fromAuthHeader(),
  //https://www.grc.com/passwords.htm generated key
  secretOrKey: process.env.JWT_SECRET
}, (payload, done) => {

  User.findById(payload._doc._id, (err, user) => {
    if(err)
      return done(err, false)
    
    if(user)
      done(null, user)
    else
      done(null, false)
  })
})

export default passportJwt