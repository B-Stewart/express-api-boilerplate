import dotenv from 'dotenv'
dotenv.config() //load env variables immediately incase anything relies upon them.
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import cors from 'cors'
import passport from 'passport'
import morgan from 'morgan'
import passportJwt from './middleware/passport-jwt'
import helmet from 'helmet'
import expressValidator from 'express-validator'

import apiRoutes from './api-routes'
import { Config } from './helpers'

const app = express()
//Middleware

//Connect mongoose
mongoose.connect(Config.MONGO_URL, (err) => {
  if(err)
    console.error('Could not connect to MongoDB...')
  else
    console.log('Connected to MongoDB...')
})

//Mongoose's promises are depricated so use global es6 promise
mongoose.Promise = global.Promise

//Advanced logging for server
if (process.env.NODE_ENV == 'production')
  app.use(morgan('common', { skip: function(req, res) { return res.statusCode < 400 }, stream: __dirname + '/morgan.log' }))
else if(process.env.NODE_ENV == 'development')
  app.use(morgan('dev'))

//Lockdown express
app.use(helmet())

//Allow for CORS, for access accross different domains. Optional but needed if using as a backend for a mobile application.
app.use(cors())

//Automatically get req.body to accept json content types (note we don't use urlencoded because of possible security leak)
app.use(bodyParser.json())
app.use(expressValidator())

// Initialize Passport session for authentication
app.use(passport.initialize())
passport.use(passportJwt)

//Set routes
for(let i = 0; i < apiRoutes.length; i++)
  app.use('/api', apiRoutes) //Add all API routes with api parent route

export default app