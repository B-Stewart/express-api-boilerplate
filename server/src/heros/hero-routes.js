import express from 'express'
import passport from 'passport'

import heroController from './hero-controller'

const routes = express.Router()

routes.get('/heros/:id', heroController.get)

routes.get('/heros', heroController.getAll)

routes.post('/heros', passport.authenticate('jwt', { session: false }), heroController.create)

routes.patch('/heros/:id', passport.authenticate('jwt', { session: false }), heroController.update)

routes.delete('/heros/:id', passport.authenticate('jwt', { session: false }), heroController.delete)

export default routes