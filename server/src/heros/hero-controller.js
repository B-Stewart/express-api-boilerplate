import Hero from './hero-model'
import { respondHelper } from '../helpers'

const controller = {}

controller.get = (req, res) => {
  Hero.findById(req.params.id).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

controller.getAll = (req, res) => {
  if(req.isAuthenticated()) {
    Hero.find({}).populate({
      path: 'city',
    }).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
  } 
  else
    respondHelper.error("Unauthorized", res)
}

controller.create = (req, res) => {
  //Validations
  req.checkBody('name', 'required').notEmpty()

  req.getValidationResult().then(validationErrors => {
    if(!validationErrors.isEmpty())
      return respondHelper.validationError(validationErrors, res)
    
    const { name, power, city } = req.body

    const hero = new Hero({
      name,
      power,
      city
    })

    hero.save().then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
  })
}

controller.update = (req, res) => {
  //Validations
  const allowedFields = ['name', 'power', 'city']
  const updatedFields = respondHelper.cleanParams(req.body, allowedFields)

  Hero.findByIdAndUpdate(req.params.id, updatedFields).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

controller.delete = (req, res) => {
  Hero.findByIdAndRemove(req.params.id).then(result => respondHelper.success(result, res)).catch(err => respondHelper.error(err, res))
}

export default controller