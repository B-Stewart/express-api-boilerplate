import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const { Schema } = mongoose

const schema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: [5, 'Name must be 5 characters or more']
  },
  power: Number,
  city: { type: Schema.ObjectId, ref: 'City' }
})

schema.plugin(uniqueValidator)

const Hero = mongoose.model('Hero', schema)

export default Hero