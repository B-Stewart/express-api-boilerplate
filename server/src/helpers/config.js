const MONGO_URL = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.NODE_ENV == 'test' ? process.env.TEST_DB_NAME : process.env.DB_NAME}`

export default {
  MONGO_URL
}