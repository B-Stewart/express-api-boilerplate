import _ from 'lodash'

let respondHelper = {}

respondHelper.success = (result, res) => {
  return res.status(200).json({
    success: true,
    data: typeof result.toJSON === 'function' ? result.toJSON() : result //We call the toJSON string to preform custom mongoose model manipulations on the display
  })
}

respondHelper.error = (err, res, status) => {
  //if err is string make it a message property
  if(_.isString(err))
    err = { message: err }
  return res.status(err.status || status || 400).json({
    success: false,
    error: err
  })
}

respondHelper.routeError = (err, req, res, next) => {
    let output = {
      success: false,
      error: {
        name: err.name,
        message: err.message,
        text: err.toString()
      }
    }
    let statusCode = err.status || 500
    return res.status(statusCode).json(output)
}

respondHelper.validationError = (validationErrorArray, res) => {
  let output = {
    success: false,
    error: { errors: validationErrorArray.array() }
  }
  return res.status(400).json(output)
}

//Use only allowed properties and remove ones that don't match... might be a better way to do this.
respondHelper.cleanParams = (obj, params) => {
  let keys = Object.keys(obj)
  for (var k in keys) {
    if (params.indexOf(k) < 0) {
      delete obj[k]
    }
  }
  return obj
}

export default respondHelper